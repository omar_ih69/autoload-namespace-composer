<?php

namespace Bitm\visitor;
class Person {
    private $first_name;
    private  $last_name;

    function __construct($first_name, $last_name)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
    }

    function __toString() {
        return $this->first_name."".$this->last_name;
    }
}
